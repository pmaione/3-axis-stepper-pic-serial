
;;; Autor    : Pedro Maione
;;; Goiania, Dezembro de 2015
    title           "Stepper Motor Functions"
    include         "def.inc"
    include         "multi-byte-macros.inc"

;;;                                   FUNCOES /  VARIAVEIS [EXTERNAS]
    EXTERN          MAIN_FLAG

;;; FUNCOES /  VARIAVEIS [GLOBAIS]
    GLOBAL          MOTOR_X_COUNTER, MOTOR_Y_COUNTER, MOTOR_Z_COUNTER, STEP_DELAY
    GLOBAL          FLAG_MOTOR_X, FLAG_MOTOR_Y, FLAG_MOTOR_Z
    GLOBAL          ResetMotors, MotorBusy
    GLOBAL          TestFLAG_MOTOR_X, TestFLAG_MOTOR_Y, TestFLAG_MOTOR_Z

__VAR_STEPPER                   UDATA
FLAG_MOTOR_X                    res     1
FLAG_MOTOR_Y                    res     1
FLAG_MOTOR_Z                    res     1

MOTOR_X_COUNTER                 res     2
MOTOR_Y_COUNTER                 res     2
MOTOR_Z_COUNTER                 res     2

__VAR_STEPPER_AUX               UDATA_SHR
STEP_DELAY                      res     1
STEP_TEMP                       res     1
;;;                                                 CODE
__SRC_STEPPER                   CODE

;;; Reseta os flag do BUFFER
;;; FLAG : Flag para ser resetado
_m_ResetMotorFlag   MACRO   FLAG
        BANKSEL     FLAG
        clrf        FLAG
        ENDM

;;; Da um passo no motor
_m_StepMotor        MACRO   MOTOR, _MOTOR_NIBBLE, _MOTOR_MASK, FLAG
        LOCAL       _local_StepMotor_CCW, _local_StepMotor_End
        BANKSEL     MOTOR
        movfw       MOTOR       ; Le o valor do PORT do MOTOR
        BANKSEL     STEP_TEMP
        movwf       STEP_TEMP   ; Armazena o estado atual do PORT do MOTOR
        swapf       STEP_TEMP,F
        movlw       _MOTOR_MASK
        andwf       STEP_TEMP,F
    #IF     _MOTOR_NIBBLE == 0
        swapf       STEP_TEMP,F
    #ENDIF
        clrc
        btfsc       FLAG,DIRECTION
        goto        _local_StepMotor_CCW
;;; _local_StepMotor_CW
        rlf         STEP_TEMP,F
        btfss       STEP_TEMP,4
        goto        _local_StepMotor_End
        movlw       0x01
        movwf       STEP_TEMP
        goto        _local_StepMotor_End

_local_StepMotor_CCW:
        rrf         STEP_TEMP,F
        bnc         _local_StepMotor_End
        movlw       0x08
        movwf       STEP_TEMP

_local_StepMotor_End:
    #IF     _MOTOR_NIBBLE == 1
        swapf       STEP_TEMP,F
    #ENDIF
        BANKSEL     MOTOR
        movfw       MOTOR
        andlw       _MOTOR_MASK
        iorwf       STEP_TEMP,W
        movwf       MOTOR
        ENDM



;;;                                         ROTINA : ResetMotors
ResetMotors:
        BANKSEL     FLAG_MOTOR_X
        clrf        FLAG_MOTOR_X
        clrf        FLAG_MOTOR_Y
        clrf        FLAG_MOTOR_Z

        BANKSEL     MOTOR_X_COUNTER
        _m_Clr16    MOTOR_X_COUNTER
        _m_Clr16    MOTOR_Y_COUNTER
        _m_Clr16    MOTOR_Z_COUNTER

        BANKSEL     _MOTORX
        clrf        _MOTORX
        clrf        _MOTORY
        clrf        _MOTORZ
        movlw       0x01
        iorwf       _MOTORX,F
        movlw       0x10
        iorwf       _MOTORY,F
        movlw       0x01
        iorwf       _MOTORZ,F
        return


;;;                                         ROTINA : MotorXStep
MotorXStep:
        _m_StepMotor _MOTORX, _MOTOR_X_NIBBLE, _MOTOR_X_MASK, FLAG_MOTOR_X
        return

MotorYStep:
        _m_StepMotor _MOTORY, _MOTOR_Y_NIBBLE, _MOTOR_Y_MASK, FLAG_MOTOR_Y
        return

MotorZStep:
        _m_StepMotor _MOTORZ, _MOTOR_Z_NIBBLE, _MOTOR_Z_MASK, FLAG_MOTOR_Z
        return

;;;                                     ROTINA : DecMOTOR_X_COUNTER
DecMOTOR_X_COUNTER:
        BANKSEL     MOTOR_X_COUNTER
        _m_Dec16    MOTOR_X_COUNTER
        skpnz
        bcf         FLAG_MOTOR_X,ENABLE
        return

DecMOTOR_Y_COUNTER:
        BANKSEL     MOTOR_Y_COUNTER
        _m_Dec16    MOTOR_Y_COUNTER
        skpnz
        bcf         FLAG_MOTOR_Y,ENABLE
        return

DecMOTOR_Z_COUNTER:
        BANKSEL     MOTOR_Z_COUNTER
        _m_Dec16    MOTOR_Z_COUNTER
        skpnz
        bcf         FLAG_MOTOR_Z,ENABLE
        return

;;;                                     ROTINA : DecMOTOR_X_COUNTER
TestFLAG_MOTOR_X:
        BANKSEL     FLAG_MOTOR_X
        btfss       FLAG_MOTOR_X,ENABLE
        goto        TestFLAG_MOTOR_X_End
        call        MotorXStep
        call        DecMOTOR_X_COUNTER
TestFLAG_MOTOR_X_End:
        return

TestFLAG_MOTOR_Y:
        BANKSEL     FLAG_MOTOR_Y
        btfss       FLAG_MOTOR_Y,ENABLE
        goto        TestFLAG_MOTOR_Y_End
        call        MotorYStep
        call        DecMOTOR_Y_COUNTER
TestFLAG_MOTOR_Y_End:
        return

TestFLAG_MOTOR_Z:
        BANKSEL     FLAG_MOTOR_Z
        btfss       FLAG_MOTOR_Z,ENABLE
        goto        TestFLAG_MOTOR_Z_End
        call        MotorZStep
        call        DecMOTOR_Z_COUNTER
TestFLAG_MOTOR_Z_End:
        return


;;;                                     ROTINA : MotorBusy
MotorBusy:
        BANKSEL     FLAG_MOTOR_X       ;
        movfw       FLAG_MOTOR_X       ;
        iorwf       FLAG_MOTOR_Y,W     ;
        iorwf       FLAG_MOTOR_Z,W     ;
        movwf       STEP_TEMP          ; Armazena temporariamente
        btfss       STEP_TEMP,ENABLE   ;
        goto        $+3                ;
        bsf         MAIN_FLAG,DEV_BUSY ;
        goto        $+2                ;
        bcf         MAIN_FLAG,DEV_BUSY ;
        return


        END

;;;                                            FIM DO ARQUIVO

