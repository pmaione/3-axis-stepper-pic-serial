;;; Autor    : Pedro Maione
;;; Goiania, Dezembro de 2015

    title           "3 stepper controller with serial comms"
    list            p=16f877a, r=hex
    errorlevel      -302, -205

    include         "def.inc"

 __CONFIG _HS_OSC & _WDTE_OFF & _PWRTE_OFF & _BOREN_ON & _LVP_OFF & _CPD_OFF & _WRT_OFF & _CP_OFF


;;; FUNCOES /  VARIAVEIS [EXTERNAS]

;;; Arquivo: usart.asm
    EXTERN          USART_TEMP
    EXTERN          SetupSerial, GetUSARTData, SendUSARTData
;;; Arquivo: buffer.asm
    EXTERN          FLAG_BUFFER_RX, FLAG_BUFFER_BLOCK
    EXTERN          ResetBUFFER_RX, ResetBUFFER_BLOCK, CopyBUFFER_RX_To_BUFFER_BLOCK
    EXTERN          WriteToBUFFER_BLOCK
;;; Arquivo: stepper.asm
    EXTERN          STEP_DELAY
    EXTERN          ResetMotors, MotorBusy, TestFLAG_MOTOR_X, TestFLAG_MOTOR_Y, TestFLAG_MOTOR_Z
;;; Arquivo: delays.asm
    EXTERN          DelayMS
;;; string-parse.asm
    EXTERN          ParseBUFFER_BLOCK
    EXTERN          ComputeFUNCTION_X, ComputeFUNCTION_Y, ComputeFUNCTION_Z, ComputeFUNCTION_F

;;; FUNCOES /  VARIAVEIS [GLOBAIS]

    GLOBAL          MAIN_FLAG, INDEX
    GLOBAL          FUNCTION_G, FUNCTION_M, FUNCTION_F, FUNCTION_X, FUNCTION_Y, FUNCTION_Z


__VAR_FUNCTION                  UDATA
FUNCTION_G                      res     1
FUNCTION_M                      res     1
FUNCTION_F                      res     2
FUNCTION_X                      res     2
FUNCTION_Y                      res     2
FUNCTION_Z                      res     2

__VAR_INTERRUPT                 UDATA_SHR
TEMP_WREG                       res     1
TEMP_STATUS                     res     1
TEMP_PCLATH                     res     1
TEMP_FSR                        res     1

__VAR_AUX                       UDATA_SHR
MAIN_FLAG                       res     1
INDEX                           res     1


RST                             CODE    0x0000
        nop                     ; Necessario para uso com DEBUG
        clrf        STATUS      ; BANK0 da memoria
        clrf        PCLATH      ; PAGINA0 da memoria de programa
        goto        START

;;; INTERRUPCAO
ISR                             CODE    0x0004
;;; Salva o contexto do microcontrolador
InterruptSaveContext:
        movwf       TEMP_WREG   ; Salva WREG
        movfw       STATUS
        movwf       TEMP_STATUS ; Salva STATUS
        movfw       PCLATH
        movwf       TEMP_PCLATH ; Salva PCLATH
        movfw       FSR
        movwf       TEMP_FSR    ; Salva FSR
        clrf        PCLATH      ; Pagina0 da memoria

;;;                                      TRATAMENTO DA INTERRUPCAO
;;; ------------------------------------------------------------------------------------------------
IntUSART_RX:
        BANKSEL     PIR1
        btfss       PIR1,RCIF    ; Ha dado no FIFO ?
        goto        EndInterrupt ; Nao
        BANKSEL     PIE1
        btfss       PIR1,RCIE    ; Interrupcoes por Rx habilitadas ?
        goto        EndInterrupt ; Nao
        ;;
        call        GetUSARTData
        ;;
;;; ------------------------------------------------------------------------------------------------
;;; FIM DA INTERRUPCAO
;;; Restaura o Contexto
EndInterrupt:
        movfw       TEMP_FSR    ;
        movwf       FSR         ; Restaura FSR
        movfw       TEMP_PCLATH ;
        movwf       PCLATH      ; Restaura PCLATH
        movfw       TEMP_STATUS ;
        movwf       STATUS      ; Restaura STATUS
        swapf       TEMP_WREG,F ;
        swapf       TEMP_WREG,W ; Restaura WREG sem afetar o STATUS
        retfie                  ; Retorna da interrupcao

;;; PROGRAMA PRINCIPAL
__SRC_MAIN                      CODE

START:
        ;; Limpa os flags (TODO: implementa dados inicializados)
        clrf        USART_TEMP
        clrf        MAIN_FLAG
        ;; Inicializa os buffers de recepcao e bloco de comando
        call        ResetBUFFER_RX
        call        ResetBUFFER_BLOCK
        ;; Chama as rotinas de configuracao
        call        SetupIO
        call        SetupSerial
        call        SetupInt
        ;; Testa os PORTS
        call        PowerUpTest
        ;; Incializa os motores
        call        ResetMotors
        call        InitMsg

MainLoop:
        BANKSEL     FLAG_BUFFER_RX
        btfss       FLAG_BUFFER_RX,EOL
        goto        MainLoop

        ;; Copia o BUFFER_RX para BUFFER_BLOCK e Reseta o BUFFER_RX
        call        CopyBUFFER_RX_To_BUFFER_BLOCK
        ;; Interpreta o BUFFER_BLOCK e Reseta o BUFFER_BLOCK

        call        ParseBUFFER_BLOCK

        ;; Interpreta as funcoes dos motores
        call        ComputeFUNCTION_X
        call        ComputeFUNCTION_Y
        call        ComputeFUNCTION_Z
        call        ComputeFUNCTION_F
        call        ToggleLEDInt

MotorLoop:
        call        TestFLAG_MOTOR_X
        call        TestFLAG_MOTOR_Y
        call        TestFLAG_MOTOR_Z

        call        MotorBusy

        btfsc       MAIN_FLAG, DEV_BUSY
        goto        MotorLoop

        BANKSEL     FLAG_BUFFER_RX
        btfss       FLAG_BUFFER_RX,EMPTY
        goto        MainLoop
        ;; O BUFFER_RX esta vazio, portanto pode receber um novo comando
        movlw       0x0A
        call        SendUSARTData
        movlw       'O'
        call        SendUSARTData
        movlw       'K'
        call        SendUSARTData

        movlw       0x1F
        call        SendUSARTData

        call        InitMsg


        goto        MainLoop


;;; ================================================================================================
;;;                                           ROTINA : SetupIO
;;;
;;; Configura entradas/saidas do microcontrolador
;;;
;;; ================================================================================================
SetupIO:
        BANKSEL     ADCON1
        movlw       0x06
        movwf       ADCON1      ; Configura os pinos do PORTA como digital I/O
        ;; ---------------------------------------------------------------------------
        ;; Configura sentido das portas do microcontrolador
        ;; ---------------------------------------------------------------------------
        movlw       0x00
        movwf       TRISA
        movlw       0x00
        movwf       TRISB
        movlw       0x00
        movwf       TRISC
        movlw       0x00
        movwf       TRISD
        movlw       0x00
        movwf       TRISE

        ;; ---------------------------------------------------------------------------
        ;; Limpa todos os registradores PORTx
        ;; ---------------------------------------------------------------------------
        BANKSEL     PORTA
        clrf        PORTA
        clrf        PORTB
        clrf        PORTC
        clrf        PORTD
        clrf        PORTE

        return
;;; ================================================================================================
;;;                                           ROTINA : SetupTMR1
;;;
;;; Configura o TMR1
;;;
;;; ================================================================================================
SetupTMR1:
        BANKSEL     T1CON
        bsf         T1CON,T1CKPS1 ; Set prescaler
        bsf         T1CON,T1CKPS0 ; to divide by 1
        bcf         T1CON,T1OSCEN ; RC Oscillator is disabled
        bcf         T1CON,TMR1CS  ; Fosc / 4

        clrf        TMR1L
        clrf        TMR1H
        bsf         T1CON,TMR1ON ; Enables TMR1

        return


;;; ================================================================================================
;;;                                           ROTINA : SetupInt
;;;
;;; Configura as interrupcoes do microcontrolador
;;;
;;; ================================================================================================
SetupInt:
        BANKSEL     PIE1
        bsf         PIE1,RCIE   ; Habilita interrupcao de recepcao USART
;;; bsf         PIE1,TXIE   ; Habilita interrupcao de transmissao USART
;;; bsf         PIE1,TMR1IE ; Habilita interrupcao para TIMER 1

        BANKSEL     INTCON
        bsf         INTCON,PEIE ; Habilita interrupcoes de perifericos
        bsf         INTCON,GIE  ; Habilita interrupcoes globais


        return
;;; ================================================================================================
;;;                                          ROTINA : ToggleLED
;;;
;;; Alterna o estado do LED_RUN (ON / OFF)
;;; ================================================================================================
ToggleLED:
        BANKSEL     PORT_LED
        btfss       LED_RUN
        goto        $+3
        bcf         LED_RUN
        goto        $+2
        bsf         LED_RUN

        return
;;; ================================================================================================
;;;                                          ROTINA : ToggleLEDInt
;;;
;;; Alterna o estado do LED_INT (ON / OFF)
;;; Indica quando entra na rotina de tratamento de interrupcao (para testes)
;;; ================================================================================================
ToggleLEDInt:
        BANKSEL     PORT_LED
        btfss       LED_INT
        goto        $+3
        bcf         LED_INT
        goto        $+2
        bsf         LED_INT

        return

;;; ================================================================================================
;;;                                          ROTINA : ToggleLED_ERRO
;;;
;;; Alterna o estado do LED_INT (ON / OFF)
;;; Indica quando entra na rotina de tratamento de interrupcao (para testes)
;;; ================================================================================================
ToggleLED_ERRO:
        BANKSEL     PORT_LED
        btfss       LED_ERRO
        goto        $+3
        bcf         LED_ERRO
        goto        $+2
        bsf         LED_ERRO

        return

PowerUpTest:
        BANKSEL     PORTA
        movlw       0xFF
        movwf       PORTA
        movwf       PORTC
        movwf       PORTD
        movlw       .200
        call        DelayMS
        clrf        PORTA
        clrf        PORTC
        clrf        PORTD
        return



InitMsg:
        clrf        INDEX
        movlw       0x0A
        call        SendUSARTData
        movlw       0x0D
        call        SendUSARTData
InitMsg_loop:
        movfw       INDEX
        call        init_msg
        call        SendUSARTData
        sublw       0x0D
        skpnz
        goto        InitMsg_end
        incf        INDEX,F
        goto        InitMsg_loop
InitMsg_end:
        return


init_msg:
        addwf       PCL,F
        dt          "CMD>>", 0x0A, 0x0D

        

        END

;;;                                            FIM DO ARQUIVO

