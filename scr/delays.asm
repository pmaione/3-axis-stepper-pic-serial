
;;;
;;; filename : delays.asm
;;; Autor    : Pedro Maione
;;; Goiania, Dezembro de 2015
;;;


  title           "Delay Functions"


;;;                                           INCLUDE HEADERS

  include         "def.inc"
  include         "math16-macros.inc"



;;; FUNCOES /  VARIAVEIS [GLOBAIS]


  GLOBAL          DelayMS



;;;                                              CONSTANTES


_XTAL_FREQ                      SET     20000000000 ; Cristal = 20MHz


;;;                                              VARIAVEIS


__VAR_DELAYS                    UDATA
DELAY_VAL                       res     1
DELAY_COUNTER                   res     2



;;;                                                 CODE

__SRC_DELAYS                    CODE


;;;
;;;                                                MACROS
;;;


;;; ================================================================================================
;;;                                                MACRO


;;;
;;;                                               ROTINAS
;;;



;;; ================================================================================================
;;;                                          ROTINA : DelayMS
;;; 
;;; ================================================================================================
DelayMS:
  #IFNDEF   __DEBUG
        BANKSEL     DELAY_VAL
        movwf       DELAY_VAL
;;; Gera um delay de 1ms (5000 instruções a 20MHz)
DelayMsLoop2:
        movlw       0x13
        movwf       DELAY_COUNTER+1
        movlw       0x88
        movwf       DELAY_COUNTER
DelayMsLoop1:
        decf        DELAY_COUNTER,F   ; Decrementa o LSByte
        skpz                        ; DELAY_COUNTER = 0 ?
        goto        DelayMsLoop1    ; Nao, decrementa novamente
        decf        DELAY_COUNTER+1,F ; Sim, Decrementa o MSByte
        skpnz                       ; DELAY_COUNTER+1 = 0 ?
        goto        EndDelayMs      ; Sim, termina o Delay
        decf        DELAY_COUNTER,F   ; Nao, decrementa DELAY_COUNTER novamente (ficara 0xFF) 
        goto        DelayMsLoop1
EndDelayMs:
        decfsz      DELAY_VAL,F ; Decrementa o Contador (n * milisegundos)
        goto        DelayMsLoop2
  #ENDIF

        
        return                          ;





        END

;;;                                            FIM DO ARQUIVO

