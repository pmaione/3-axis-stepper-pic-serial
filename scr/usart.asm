;;; Autor    : Pedro Maione
;;; Goiania, Dezembro de 2015
  title           "USART Functions"
  include         "def.inc"
;;; FUNCOES / VARIAVEIS [EXTERNAS]
  EXTERN          FLAG_BUFFER_RX
  EXTERN          WriteToBUFFER_RX

;;; FUNCOES /  VARIAVEIS [GLOBAIS]
  GLOBAL          USART_TEMP
  GLOBAL          SetupSerial, GetUSARTData, SendUSARTData

;;;                                              CONSTANTES
_BRGH_VAL                       SET     1
_BAUD_RATE_GEN                  SET     .64

;;;                                              VARIAVEIS
__VAR_USART                     UDATA_SHR
USART_TEMP                      res     1

;;; ----------------------------------------------------------------------------
__SRC_USART                     CODE

;;;                                         ROTINA : SetupSerial
;;;
SetupSerial:
        BANKSEL     TRISC          ;
        movlw       0xC0           ;
        iorwf       TRISC,F        ;
        movlw       _BAUD_RATE_GEN ;
        movwf       SPBRG          ; Carrega o SPBRG com o valor adequado
        bsf         TXSTA, TXEN    ; Habilita transmissao de dados
  #IF       _BRGH_VAL == 01        ;
        bsf         TXSTA, BRGH    ; Modo Assincrono de alta velocidade
  #ELSE                            ;
        bcf         TXSTA,BRGH     ; Modo Assincrono de baixa velocidade
  #ENDIF                           ;
        BANKSEL     RCSTA          ;
        bsf         RCSTA, CREN    ; Habilita Recepcao Continua
        bsf         RCSTA, SPEN    ; Habilita Porta Serial (RC7/RX; RC6/TX)
        ;; Limpa o FIFO de Recepcao
        movfw       RCREG       ;
        movfw       RCREG       ;
        movfw       RCREG       ;
        clrw                    ; Limpa WREG
        ;;
        return

;;;                                        ROTINA : GetUSARTData
;;;
;;; Recebe um dado pela porta serial.
;;; Metodo: Interrupcao
;;;
GetUSARTData:
        BANKSEL     RCSTA
        btfsc       RCSTA,OERR  ; Erro de Overrun
        goto        ErroOverrun ; Sim
        btfsc       RCSTA,FERR  ; Erro de Framing
        goto        ErroFraming ; Sim
        BANKSEL     RCREG
        movfw       RCREG       ; Pega o dado
        movwf       USART_TEMP  ; Armazena temporariamente
        ;; Verifica se o buffer esta cheio
        BANKSEL     FLAG_BUFFER_RX
        btfsc       FLAG_BUFFER_RX,FULL
        goto        ErroRxFULL
        ;; Verifica se o dado recebido eh o EOL
        movlw       _CHAR_RX_EOL       ;
        xorwf       USART_TEMP,W       ;
        skpnz                          ; USART_TEMP == _CHAR_RX_EOL ?
        bsf         FLAG_BUFFER_RX,EOL ; Sim, seta o flag
        movfw       USART_TEMP
        call        WriteToBUFFER_RX ; Escreve o dado no buffer
        movfw       USART_TEMP
        call        SendUSARTData ; Envia o dado para conferencia
        BANKSEL     FLAG_BUFFER_RX
        btfsc       FLAG_BUFFER_RX,EMPTY ;
        bcf         FLAG_BUFFER_RX,EMPTY ; Limpa o flag
        return


;;; Erro de 'Overrun' detectado
;;; Acontence quando a FIFO esta cheia e recebe um dado
ErroOverrun:
        ;; Envia o caractere para o software host indicando erro
        _m_SetErrorLED
        movlw       _CHAR_ER_RX_OVER
        call        SendUSARTData
        ;; Simplemente reinicia a recepcao continua
        bcf         RCSTA,CREN  ;
        bsf         RCSTA,CREN  ; Reabilita recepcao continua
        return

;;; Erro de 'Framing'
;;; Acontece quando o STOP bit foi detectado como 0 (deveria ser 1)
ErroFraming:
        ;; Envia o caractere para o software host indicando erro
        _m_SetErrorLED
        movlw       _CHAR_ER_RX_FRAM
        call        SendUSARTData
        movfw       RCREG       ; Descarta o dado recebido
        return

;;; Erro Buffer de Recepcao Cheio
ErroRxFULL:
        _m_SetErrorLED
        movlw       _CHAR_ER_RX_BUFF_FULL
        call        SendUSARTData
        movfw       RCREG              ; Descarta o dado recebido
        xorlw       _CHAR_RX_EOL       ;
        skpnz                          ; Testa se nao eh o caractere de fim de comando
        bsf         FLAG_BUFFER_RX,EOL ;
        ;; O buffer Rx esta cheio e nao chegou ao fim do comando
        ;; Implementar mensagem de erro para ser retornada
        return

;;; ================================================================================================
;;;                                        ROTINA : SendUSARTData
;;;
;;; Envia o dado em WREG pela porta serial
;;; Metodo: Polling
;;;
;;; ================================================================================================
SendUSARTData:
        BANKSEL     PIR1
        btfss       PIR1,TXIF   ;
        goto        $-1         ; Aguarda o buffer de transm. USART esvaziar
        BANKSEL     TXREG
        movwf       TXREG       ; Transmite o dado
        BANKSEL     TXSTA
        btfss       TXSTA,TRMT
        goto        $-1         ; Aguarda o TRANSMIT SHIFT REGISTER esvaziar
        return

  END
